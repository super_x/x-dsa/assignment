//WAP to find factorial of given number.

import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a number:");
		int num = Integer.parseInt(br.readLine());

		int fact = 1;

		for(int i = 2; i<=num; i++){

			fact = fact * i;
		}

		System.out.println(fact);
	}
}
