/*
A b C d E 
e D c B 
B c D 
d C 
C 
*/

class Demo{

	static void Demo(int row){
		
		int x = 1; 
		for(int i = 1; i<=row; i++){

			for(int j = 1; j <= row - i+1; j++){

				if(i%2 == 1){

					if(j%2 == 1){
						System.out.print((char)(x+64)+" ");
					}else{
						System.out.print((char)(x+96)+" ");
					}
					x++;
				}else{
					if(j%2==1){
						System.out.print((char)(x+96)+" ");
					}else{
						System.out.print((char)(x+64)+" ");
					}
					x--;
				}
			}
			System.out.println();
			if(i%2==1)
				x--;
			else
				x++;
		}


	}	

	public static void main(String[] args){


		Demo(5);

	}
}
