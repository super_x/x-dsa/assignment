class Demo{

	static void fun(int num){

		int rev = 0;

		while(num != 0){

			rev = rev * 10 +(num % 10);
			num/=10;
		
		}

		while(rev != 0){

			int rem = rev%10;

			if(rem % 2 == 0){
				
				int fact  = 1;

				for(int	i = rem; i>= 1; i--){

					fact *= i;
				}
				System.out.println(fact);
			}
			rev/=10;
		}
	}

	public static void main(String[] args){

		fun(12345);
	}
}
