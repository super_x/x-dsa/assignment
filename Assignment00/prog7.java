
import java.util.Scanner;

class Demo{

	static void fun(int arr[],int size){
		
		int arr2[] = new int[size];
	
		for(int i = 0; i<size; i++){

			if(arr[i] % 2 == 0){
				arr2[i] = 1;
			}
		}

		for(int x:arr2){
			System.out.print(x);
		}

		System.out.println();
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the size of array");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the elements of array");
		for(int i = 0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		fun(arr,arr.length);
	}
}




