//successive sum of the digits of the integer by reversing it


class Demo{

	static void SuccessiveSum(int num){

		int x = num;
		int count = 0;

		while(x != 0){

			count++;
			x/=10;
		}

		int arr[] = new int[count];
		for(int i = 0; i<arr.length; i++){

			arr[i] = num%10;
			num/=10;
		}

		for(int i = 0; i<arr.length - 1; i++){

			arr[i] = arr[i+1] + arr[i];
		}

		for(int i : arr){
			System.out.println(i);
		}

	}	

	public static void main(String[] args){

		SuccessiveSum(45689);

	}
}
