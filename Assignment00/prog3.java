/*
D C B A 
e f g h 
F E D C 
g h i j 
*/

class Demo{

	static void Pattern(int rows){

		for(int i = 1; i<=rows; i++){

			int ch = rows + i - 1;

			for(int j =1; j<= rows; j++){

				if(i%2 == 1){
					System.out.print((char)(ch-j+65)+" ");
				}else{
					System.out.print((char)(ch+j+95)+" ");
				}
			}


			System.out.println();
		}

	}	

	public static void main(String[] args){


		Pattern(4);

	}
}
