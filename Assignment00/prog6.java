//check the digit with previous if the previous digit  is greater than current return that num


class Demo{

	static int Numcheck(int num){

		int x = num;
		int count = 0;

		while(x != 0){

			count++;
			x/=10;
		}

		int arr[] = new int[count];
		
		for(int i = 0; i<arr.length; i++){

			arr[i] = num%10;
			num/=10;
		}

		for(int i = 1; i<arr.length; i++){

			if(arr[i-1] > arr[i])
				return arr[i-1];
		}
		return -1;

	}	

	public static void main(String[] args){

		System.out.println(Numcheck(4569759));

	}
}
