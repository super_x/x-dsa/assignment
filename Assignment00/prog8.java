//cout the occurance of vowels in the string


import java.util.*;
class Demo{

	static void Vcount(String str){

		int a = 0;
		int e = 0;
		int i = 0;
		int o = 0;
		int u = 0;

		for(int x = 0; x<str.length(); x++){

			char ch = str.charAt(x);

			if(ch == 'a' || ch == 'A')
				a++;
			else if(ch == 'e' || ch =='E')
				e++;
			else if(ch == 'i' || ch == 'I')
				i++;
			else if(ch == 'o' || ch == 'O')
				o++;
			else if(ch == 'u' || ch == 'U')
				u++;
		}

		System.out.println("a = :"+a);
		System.out.println("e = :"+e);
		System.out.println("i = :"+i);
		System.out.println("o = :"+o);
		System.out.println("u = :"+u);
	}

				

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string");
		String str = sc.next();

		Vcount(str);
	}
}
