//palindrome string


import java.util.*;
class Demo{

	static void Palindrome(String str){
	
		char carr[] = new char[str.length()];
		carr = str.toCharArray();

		int s = 0; 
		int e = str.length() - 1;

		while(s<e){
			
			System.out.println(carr[s]);
			System.out.println(carr[e]);
			if(carr[s] != carr[e]){

				System.out.println("String is not palindrome");
				return;
			}

			s++;
			e--;
		}
		System.out.println("String is palindrome");
		
	}

				

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string");
		String str = sc.next();

		Palindrome(str);
	}
}
