/*

Que 5: WAP to toggle the String to uppercase or lowercase
Input: Java output: jAVA
Input: data output: DATA

*/

import java.io.*;

class Demo{

	static String Toggle(String str){

		char arr[] = new char[str.length()];
		arr = str.toCharArray();

		for(int i = 0; i<arr.length; i++){

			if(arr[i] < 97){
				arr[i] = (char)(arr[i] + 32);
			}else
				arr[i] = (char)(arr[i] - 32);
		}

		return new String(arr);
	}



	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();

		str = Toggle(str);
		System.out.println(str);
	}
}
