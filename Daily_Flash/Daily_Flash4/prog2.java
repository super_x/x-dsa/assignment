
import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter rows:");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){

			for(int j = i; j>=1; j--){

				System.out.print((char)(64+j)+" ");
			}
			System.out.println();
		}
	}
}

/*

A 
B A 
C B A 
D C B A 

*/
