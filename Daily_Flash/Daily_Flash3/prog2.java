/*

1 
2 1 
3 2 1 
4 3 2 1

*/


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter row :");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<= row; i++){

			for(int j = i; j>=1; j--){
				
				System.out.print(j+" ");
			}
			

			System.out.println();
		}
	}
}
