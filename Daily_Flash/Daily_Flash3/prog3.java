

//check whether the given number is palindrome or not


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a number :");
		int num = Integer.parseInt(br.readLine());
		int num1 = num;
		int rev = 0;

		while(num!=0){
			rev = rev*10+(num%10);
			num/=10;
		}

		if(rev == num1){
			System.out.println("Number is palindrome");
		}else
			System.out.println("Number is not palindrome");
	}
}
