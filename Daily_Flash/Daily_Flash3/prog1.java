/*

A B C D 
D C B A 
A B C D 
D C B A

*/


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter row :");
		int row = Integer.parseInt(br.readLine());

		char ch = 'A';

		for(int i = 1; i<= row; i++){

			for(int j = 1; j<=row; j++){
				
				if(i%2 == 1){
					System.out.print(ch + " ");
					ch ++;
				}else{
					System.out.print(ch + " ");
					ch--;
				}
			}
			if(i%2 == 1)
				ch--;
			else
				ch++;

			System.out.println();
		}
	}
}
