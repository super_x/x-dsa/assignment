

//Que 5 : WAP to check whether the string contains characters other than
//letters.


import java.io.*;

class Demo{

	static boolean Check(String str){

		boolean a = false;

		for(int i = 0; i<str.length(); i++){
			
			char ch = str.charAt(i);
			System.out.println(ch);

			if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')){
				a = true;
				continue;
			}else{
				a = false;
				break;
			}
	
		}
		return a;
	}


	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a string :");
		String str = br.readLine();

		boolean a = Check(str);

		if(a == true)
			System.out.println("No string dosn't contains characters other than letters");
		else
			System.out.println("Yes string contains characters other than letters");
	}
}
