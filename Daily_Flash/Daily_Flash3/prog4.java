
/*

Que 4 : WAP to print each reverse numbers in the given range
Input: start:25435
end: 25449

*/

class Demo{

	public static void main(String[] args){

		for(int i = 25435; i<=25449; i++){
			
			int rev = 0;
			int num = i;

			while(num != 0){

				rev = rev * 10+(num%10);
				num/=10;
			}

			System.out.println(rev);
		}
	}
}
