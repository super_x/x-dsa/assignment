
//check whether the given number is perfect or not

import java.util.*;

class Demo{

	static boolean IsPerfect(int num){
		
		int sum = 0;
		for(int i = 1; i<=num/2; i++){
			
			if(num%i == 0)
				sum += i;
		}

		if(sum == num)
			return true;
		else
			return false;
	}



	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a number");
		int num = sc.nextInt();
	
		boolean ret = IsPerfect(num);
		
		if(ret == false)
			System.out.println(num +" is not a perfect");
		else
			System.out.println(num +" is perfect");
	}
}
