
//print strong numbers in given range.

import java.util.*;

class Demo{

	static void IsStrong(int start, int end){
		
		for(int i = start; i<=end; i++){

			int num = i;
			int sum = 0;

			while(num!=0){

				int digit = num%10;
				int fact = 1;
				for(int j = 1; j<=digit; j++){
					
					fact*=j;
				}
				sum += fact;
				num /= 10;
			}

			if(sum == i)
				System.out.println(i);
		}
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a start");
		int start = sc.nextInt();

		System.out.println("Enter end");
		int end = sc.nextInt();
	
		IsStrong(start,end);
	}
}
