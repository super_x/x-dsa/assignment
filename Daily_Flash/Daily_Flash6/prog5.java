//replace vowels to # in given string


import java.io.*;
class Demo{

	static String replace(String str){

		char arr[] = new char[str.length()];
		arr = str.toCharArray();

		for(int i = 0; i<arr.length; i++){

			if(arr[i] == 'a' || arr[i] == 'A' || arr[i] == 'e' || arr[i] == 'E'|| arr[i] == 'i' || arr[i] == 'I'|| arr[i] == 'o' || arr[i] == 'O'|| arr[i] == 'u' || arr[i] == 'U')
				arr[i] = '#';
		}

		return new String(arr);
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string");
		String str = br.readLine();
		
		str = replace(str);

		System.out.println(str);
	}
}
