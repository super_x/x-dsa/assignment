/*
1 
7	26	
63	124	215	
342	511	728	999
*/

import java.util.*;

class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the row");
		int row = sc.nextInt();
		int num  = 1;

		for(int i = 1; i<= row; i++){

			for(int j = 1; j<= i; j++){
				if(num == 1){
					System.out.print("1 ");
				}else{
					System.out.print(num*num*num-1+"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}
