/*
A	B	C	D	
1	3	5	7	
A	B	C	D	
9	11	13	15	
A	B	C	D
*/

import java.util.*;

class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the row");
		int row = sc.nextInt();

		System.out.println("Enter the row");
                int col = sc.nextInt();

		int num = 1;

		for(int i = 1; i<= row; i++){

			for(int j = 1; j<= col; j++){

				if(i%2 == 1)
					System.out.print((char)(64+j)+ "\t");
				else {
					System.out.print(num + "\t");
					num= num + 2;
				}	
			}
			System.out.println();
		}
	}
}
