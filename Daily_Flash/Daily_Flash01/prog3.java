/*

1 
1 2 
2 3 4 
5 6 7

*/


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a number :");
		int num = Integer.parseInt(br.readLine());
	
		if(num % 2 == 0)
			System.out.println("Given number is even");
		else
			System.out.println("Given number is odd");
	}
}
