/*

1 2 3 4 
2 3 4 5 
3 4 5 6 
4 5 6 7

*/


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter row :");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<= row; i++){
			
			int num = i;

			for(int j = 1; j<=row; j++){
				
				System.out.print(num++ +" ");
			}

			System.out.println();
		}
	}
}
