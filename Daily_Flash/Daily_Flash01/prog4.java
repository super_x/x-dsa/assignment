/*

WAP to print the odd numbers in the given range
Input: start:1
end:10

*/



class Demo{

	public static void main(String[] args){
	
		for(int i = 1; i<= 10; i++){

			if(i % 2 == 0)
				System.out.println("Given number is even");
			else
				System.out.println("Given number is odd");
		}
	}
}
