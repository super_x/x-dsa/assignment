/*

Que 5 : WAP to count the size of given string
(without using inbuilt method)

*/


import java.io.*;

class Demo{

	static int mylength(String str){

		int size = str.length();

		char ch[] = new char[size];
		ch = str.toCharArray();

		int count = 0;
		for(int i = 0; i<ch.length; i++){
			count ++;
		}

		return count;
	}


	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a string :");
		String str = br.readLine();
		
		System.out.println(mylength(str));

	}
}
