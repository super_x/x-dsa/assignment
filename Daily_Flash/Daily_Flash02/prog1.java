/*

A B C D
B C D E
C D E F
D E F G

*/


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter row :");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<= row; i++){
			
			char ch = (char)('A' + i - 1);

			for(int j = 1; j<=row; j++){
				
				System.out.print(ch + " ");
				ch++;
			}

			System.out.println();
		}
	}
}
