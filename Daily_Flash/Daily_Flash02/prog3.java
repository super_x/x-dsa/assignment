
/*

: WAP to check whether the given no is prime or composite

*/


import java.io.*;

class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter a number :");
		int num = Integer.parseInt(br.readLine());

		int count = 0;

		for(int i = 1; i*i <= num; i++){
			
			if(num%i == 0)
				count ++;
		}

		System.out.println(count);

		if(num == 1)
			System.out.println("1 is not prime not composite");

		else if(count == 1)
			System.out.println("num is prime");

		else if(count > 1)
			System.out.println("num is composite");
	}
			
}
