/*

Que 4 : WAP to print the composite numbers in the given range
Input: start:1
end:100

*/

class Demo{

	public static void main(String[] args){

		for(int i = 1; i<= 100; i++){
			int count = 0;
			for(int j = 1 ; j*j <= i; j++){
				
				if(i%j == 0)
					count ++;
			
			}

			if(count >1)	
				System.out.println(i+" is composite");
		}
		
	}
}
