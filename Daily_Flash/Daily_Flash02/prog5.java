/*

Que 5 : WAP to check whether the string contains vowels and return
the count of vowels.

*/
import java.io.*;

class Demo{

	static int VowelsCount(String str){

		int count = 0;

		for(int i = 0; i<str.length(); i++){

			char ch = str.charAt(i);

			if(ch == 'a' || ch == 'A' || ch == 'e' || ch == 'E' || ch == 'i' || ch == 'I' || ch == 'o' || ch =='O' || ch == 'u' || ch == 'U')
				count ++;
		}
		return count;
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter a string :");
		String str = br.readLine();
		
		System.out.println(VowelsCount(str));
		
	}
}
