/*
Que 1: WAP to print the factorial of digits in a given range.
Input: 1-10
*/

class Demo{

	public static void main(String[] args){

		for(int i = 1; i<=10; i++){
			
			int fact = 1;
			for(int j=1; j<=i; j++){

				fact = fact * j;

			}

			System.out.println(fact);
		}
	}
}

