/*

Que 5: WAP to print the occurrence of a letter in given String.
Input String: “Know the code till the core”
Alphabet : o
Output: 3

*/

class Demo{

	public static void main(String[] args){
		
		String str = "Know the code till the core";

		int count = 0;
		for(int i = 0; i<str.length(); i++){

			if(str.charAt(i) == 'o')
				count ++;
		}
		System.out.println(count);
	}
}

