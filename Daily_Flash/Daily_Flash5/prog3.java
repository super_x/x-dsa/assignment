/*

Que 3: WAP to check whether the given number is a strong number or not.

*/

import java.util.*;

class Demo{
	
	static void Fact(int num){
		
		int sum = 0;
		int num1 = num;

		while(num != 0){

			int mult = 1;
			for(int i = 1; i<= num%10; i++)
				mult = mult * i;

			sum = sum + mult;
			num/=10;
		}
		
		System.out.println(sum);

		if(sum == num1)
			System.out.println(num1+" is strong number");
		else
			System.out.println(num1+" is not a strong number");
	}

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a number");
		
		int num = sc.nextInt();
		Fact(num);
	}
}

