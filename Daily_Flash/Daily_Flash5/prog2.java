/*

Que 2: WAP to print the following pattern
Take row input from the user

a
A B
a b c
A B C D

*/

class Demo{

	public static void main(String[] args){

		for(int i = 1; i<=4; i++){
		
			for(int j=1; j<=i; j++){

				if(i%2==1)
					System.out.print((char)('a'+j-1)+" ");
				else
					System.out.print((char)('A'+j-1)+" ");

			}

			System.out.println();
		}
	}
}

